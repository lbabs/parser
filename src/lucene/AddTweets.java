/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lucene;

import java.io.IOException;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import Constructors.Tweets;

/**
 *
 * @author lebab
 *
 * This class is used to add Tweets to be indexed from lucene
 * @
 */
public class AddTweets {

    /**
     *
     * @param w The parameter of IndexWriter used by lucene
     * @param mdoc Call Tweets in order to get username and tweets and add them
     * to Indexwriter so as to index them
     * @throws IOException
     */
    public void addDoc(IndexWriter w, Tweets mdoc) throws IOException {
        Document doc = new Document();
        doc.add(new StringField("Username", mdoc.getUsername(), StringField.Store.YES));
        doc.add(new Field("Tweet", mdoc.getTweet(), TextField.TYPE_STORED));
        w.addDocument(doc);
    }
}
