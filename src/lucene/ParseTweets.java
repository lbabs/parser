package lucene;

import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.math.BigInteger;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import parser.DateFormat;
import Constructors.Tweets;
import java.io.BufferedReader;
import java.io.FileNotFoundException;

/**
 *
 * @author lebab
 */
public class ParseTweets {

    public List<Tweets> parseDocFile(String file) throws ParseException, FileNotFoundException, IOException {

        /**
         * It is the list that contains all the data and one can have access to
         * its elements
         */
        List<Tweets> AllTweets = new ArrayList<>();
        try {

            /**
             * A list that contains all the unique dates
             */
            List<String> unique_dates = new ArrayList<>();
            Collections.sort(unique_dates);

            /**
             * Contains username => and the all data of this user
             */
            HashMap<String, List<Tweets>> un_contents = new HashMap<>();

            /**
             * Contains dates and the encoded dates
             */
            HashMap<String, Integer> IndexDate = new HashMap<>();

            /**
             * Initialize buffer so as to read the file
             */
            BufferedReader in = new BufferedReader(new FileReader(file));
            {

                String str = in.readLine();

                /**
                 * Read the file until the end
                 */
                while (str != null) {


                    /**
                     * Initialize of the fields
                     */
                    int polarity = 0;
                    BigInteger id = null;
                    String date = "";
                    String query = "";
                    String username = "";
                    String tweet = "";

                    /**
                     * Seperate 6 different fields
                     */
                    List<String> seperated_fields = new ArrayList<>();
                    String[] str2Tokens;
                    str2Tokens = str.trim().split(",\"");

                    for (int i = 0; i < str2Tokens.length; i++) {
                        seperated_fields.add(str2Tokens[i].replace("\"", ""));
                    }

                    /**
                     * Every field takes the proper value from dataset
                     */
                    for (int j = 0; j < seperated_fields.size(); j++) {
                        polarity = Integer.parseInt(seperated_fields.get(0));
                        id = new BigInteger(seperated_fields.get(1));
                        date = str2Tokens[2];
                        username = str2Tokens[4];
                        tweet = str2Tokens[5];
                    }

                    /**
                     * Removes (") from the data
                     */
                    if (tweet.contains("\"") || username.contains("\"")) {
                        tweet = tweet.replace("\"", "");
                        username = username.replace("\"", "");
                    }

                    /**
                     * Parse of dates. Through DataFormat we give a pattern
                     * dd/mm/yyyy
                     */
                    DateFormat df = new DateFormat();
                    if (date != null) {
                        date = df.convert(date);
                        Tweets t = new Tweets(polarity, id, date, query, username, tweet);
                        AllTweets.add(t);
                    }
                    str = in.readLine();
                }
            }


            for (int k = 0; k < AllTweets.size(); k++) {
                Tweets tw = AllTweets.get(k);
                String un = tw.getUsername();
                String d = tw.getDate();


                /**
                 * Adds dates that are not in the list
                 */
                if (!unique_dates.contains(d)) {
                    unique_dates.add(d);
                }

                /**
                 * If exists, we add them to tweet
                 */
                if (un_contents.containsKey(un)) {
                    List<Tweets> list = un_contents.get(un);
                    list.add(tw);
                    un_contents.put(un, list);

                } else {
                    List<Tweets> list = new ArrayList<>();
                    list.add(tw);
                    un_contents.put(un, list);
                    Collections.sort(list, new Comparator() {
                        @Override
                        public int compare(Object t, Object t1) {
                            Date s1 = (Date) t;
                            Date s2 = (Date) t1;
                            long n1 = s1.getTime();
                            long n2 = s2.getTime();
                            if (n1 < n2) {
                                return -1;

                            } else if (n1 > n2) {
                                return 1;
                            } else {
                                return 0;
                            }
                        }
                    });
                }
            }

            /**
             * Initialization of the file that contains the data that would be
             * selected next
             */
            FileOutputStream out;
            PrintStream ps;
            out = new FileOutputStream("./data\\data.txt");
            ps = new PrintStream(out);


            Set<String> usernames = un_contents.keySet();
            Iterator it = usernames.iterator();
            while (it.hasNext()) {
                String un2 = (String) it.next();
                List<Tweets> list2 = un_contents.get(un2);
                int label = 0;
                List<String> dates = new ArrayList<>();

                /**
                 * Creation of label
                 */
                for (int j = 0; j < list2.size(); j++) {
                    String date = list2.get(j).getDate();
                    dates.add(date);
                }
                for (int y = 0; y < dates.size(); y++) {
                    String date = list2.get(y).getDate();
                    label = unique_dates.indexOf(date);
                    String s = unique_dates.get(label);
                    if (s.equals(dates.get(y))) {
                        String date_index = s;
                        IndexDate.put(date_index, label);
                    }
                }
            }

            /**
             * Data are fetched and written in ./data/data.txt
             */
            for (int i = 0; i < AllTweets.size(); i++) {
                int polarity = AllTweets.get(i).getPolarity();
                BigInteger id = AllTweets.get(i).getId();
                String date = AllTweets.get(i).getDate();
                String username = AllTweets.get(i).getUsername();
                String tweet = AllTweets.get(i).getTweet();
                Set keys = IndexDate.keySet();

                //Choose the data
                for (Iterator iter = keys.iterator(); iter.hasNext();) {
                    String key = (String) iter.next();
                    int value = IndexDate.get(key);

                    /**
                     * Here we assign ti each date a unique code and print it in
                     * a new file
                     */
                    if (date.equals(key)) {
                        ps.println(polarity + "\t" + id + "\t" + value + "\t" + key + "\t" + username + "\t" + tweet);
                    }
                }
            }

        } catch (IOException e) {

            System.out.println(e);
        }
        return AllTweets;
    }
}
