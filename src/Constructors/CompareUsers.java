package Constructors;

/**
 *
 * @author lebab
 */
public class CompareUsers {

    String username;
    String codedate;
    String date;
    String tweets;
    String polarity;
    float score;

    /**
     *
     * @param un The username of the user
     * @param cdt The date coded
     * @param dat The date of a single tweet
     * @param tw The tweet of the user
     */
    public CompareUsers(String un, String cdt, String dat, String tw) {

        this.username = un;
        this.codedate = cdt;
        this.date = dat;
        this.tweets = tw;

    }

    /**
     *
     * @param un The username of the user
     * @param cdt The date coded
     * @param dat The date of a single tweet
     * @param tw The tweet of the user
     * @param pol If a tweet is positive, negative, neutral
     */
    public CompareUsers(String un, String cdt, String dat, String tw, String pol) {

        this.username = un;
        this.codedate = cdt;
        this.date = dat;
        this.tweets = tw;
        this.polarity = pol;

    }

    /**
     *
     * @param un The username of the user
     * @param tw The tweet of the user
     */
    public CompareUsers(String un, String tw) {
        this.username = un;
        this.tweets = tw;
    }

    /**
     *
     * @param un The username of the user
     * @param tw The tweet of the user
     * @param sc The similarity between two tweets
     */
    public CompareUsers(String un, String tw, Float sc) {
        this.username = un;
        this.tweets = tw;
        this.score = sc;
    }

    /**
     *
     * Getters and setters
     */
    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setCodedate(String codedate) {
        this.codedate = codedate;
    }

    public String getCodedate() {
        return codedate;
    }

    public void setTweets(String tweets) {
        this.tweets = tweets;
    }

    public String getTweets() {
        return tweets;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public String getPolarity() {
        return polarity;
    }
}