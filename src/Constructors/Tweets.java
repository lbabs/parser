package Constructors;

/**
 *
 * @author lebab
 */
import java.math.BigInteger;

/**
 *
 * @author lebab
 */
public class Tweets {

    private int polarity;
    private BigInteger id;
    private String date;
    private String query;
    private String username;
    private String tweet;
    private float score;

    /**
     *
     * @param pol The polarity of tweet
     * @param ident The ID of each user - It could be really a big number -
     * @param d Date of tweet
     * @param q Main theme of tweet
     * @param un The username
     * @param t Tweet of user
     */
    public Tweets(int pol, BigInteger ident, String d, String q, String un, String t) {
        this.polarity = pol;
        this.id = ident;
        this.date = d;
        this.query = q;
        this.username = un;
        this.tweet = t;
    }

    /**
     *
     * @param tw Tweet of user
     * @param sc The score - float - between two tweets (similarity)
     */
    public Tweets(String tw, float sc) {
        this.tweet = tw;
        this.score = sc;
    }

    /**
     *
     * @param un The username
     * @param tw Tweet of user
     */
    public Tweets(String un, String tw) {
        this.username = un;
        this.tweet = tw;
    }

    /**
     *
     * @param un The username
     * @param tw Tweet of user
     * @param sc The score - float - between two tweets (similarity)
     */
    public Tweets(String un, String tw, float sc) {
        this.username = un;
        this.tweet = tw;
        this.score = sc;
    }

    public Tweets(float sc) {
        this.score = sc;
    }

    public void setPolarity(int polarity) {
        this.polarity = polarity;
    }

    public int getPolarity() {
        return polarity;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public BigInteger getId() {
        return id;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getQuery() {
        return query;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setTweet(String tweet) {
        this.tweet = tweet;
    }

    public String getTweet() {
        return tweet;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public float getScore() {
        return score;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Tweets other = (Tweets) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
}