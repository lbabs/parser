package parser;

import Constructors.CompareUsers;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Lefteris
 */
public class HelpClass {

    /**
     *
     * @param parseData A list of that holds list of data
     * @return A Hashmap that has as a key the username and as a value the data
     * of the user for the whole dataset
     */
    public Map<String, List<CompareUsers>> UserTweets(List<CompareUsers> parseData) {
        String un = "";
        String tw = "";
        Map<String, List<CompareUsers>> userTweets = new HashMap<String, List<CompareUsers>>();
        for (int m = 0; m < parseData.size(); m++) {
            un = parseData.get(m).getUsername();
            tw = parseData.get(m).getTweets();

            if (userTweets.containsKey(un)) {
                userTweets.get(un).add(parseData.get(m));
            } else {
                List<CompareUsers> newUserTweets = new LinkedList<CompareUsers>();
                newUserTweets.add(parseData.get(m));
                userTweets.put(un, newUserTweets);
            }
        }
        return userTweets;
    }
}