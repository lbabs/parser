package parser;

import Constructors.CompareUsers;
import Constructors.Tweets;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import lucene.TweetSimilarity;

/**
 *
 * @author lebab
 */
public class ValueDistance {

    /**
     *
     * @param data A list that holds the data of the first user
     * @param data2 A list that holds the data of the second user
     * @return A float number which is the score - the similarity of the two
     * users
     * @throws ParseException
     * @throws FileNotFoundException
     * @throws IOException
     * @throws org.apache.lucene.queryparser.classic.ParseException
     */
    public float compute(List<CompareUsers> data, List<CompareUsers> data2) throws ParseException, FileNotFoundException, IOException, org.apache.lucene.queryparser.classic.ParseException {


        /**
         * Initialization of an array that holds the values in order to compute
         * the modified edit distance
         */
        float[][] dp = null;

        float p = 0.0f;
        dp = new float[data.size() + 1][data2.size() + 1];

        for (int i = 0; i < dp.length; i++) {
            for (int j = 0; j < dp[i].length; j++) {
                dp[i][j] = i == 0 ? j : j == 0 ? i : 0;
                if (i > 0 && j > 0) {
                    p = content(data.get(i - 1).getTweets(), data.get(i - 1).getUsername(), data2.get(j - 1).getTweets(), data2.get(j - 1).getUsername());
                    dp[i][j] = Math.min(Math.min(dp[i][j - 1] + 1, dp[i - 1][j] + 1), (dp[i - 1][j - 1] + ((data.get(i - 1).getCodedate().equals(data2.get(j - 1).getCodedate())) ? p : 1)));
                }
            }
        }
        return dp[data.size()][data2.size()];
    }

    /**
     *
     * @param tweet1 Tweet of the first user
     * @param un1 Username of the first user
     * @param tweet2 Tweet of the second user
     * @param un2 Username of the second user
     * @return The score between tweets of a user - float -
     * @throws ParseException
     * @throws FileNotFoundException
     * @throws IOException
     * @throws org.apache.lucene.queryparser.classic.ParseException
     */
    public float content(String tweet1, String un1, String tweet2, String un2) throws ParseException, FileNotFoundException, IOException, org.apache.lucene.queryparser.classic.ParseException {

        TweetSimilarity ts = new TweetSimilarity();
        List<Tweets> cp = ts.Tweet_Similarity(tweet1, tweet2, un1);
        float score = 0.0f;
        for (int f = 0; f < cp.size(); f++) {
            if (!un1.equals(cp.get(f).getUsername())) {
                score = cp.get(f).getScore();
            }
        }
        return score;
    }
}