package parser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Converts date
 *
 * @author lebab
 */
public class DateFormat {

    /**
     *
     * @param date The original date
     * @return A list of date. Format: dd/MMM/yyy
     * @throws ParseException
     */
    public String convert(String date) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.ENGLISH);
        Date dateStr = formatter.parse(date);
        date = formatter.format(dateStr);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyy", Locale.ENGLISH);
        date = sdf.format(dateStr);
        return date;
    }
}