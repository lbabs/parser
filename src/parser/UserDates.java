/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package parser;

import Constructors.CompareUsers;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lebab
 */
public class UserDates {

    //Dimiourgia tou parser.
    /**
     * 
     * @param file The file that the parser reads
     * @return A List of all the data of each user in the dataset
     * @throws ParseException
     * @throws FileNotFoundException 
     */
    public List<CompareUsers> parseData(String file) throws ParseException, FileNotFoundException {


        //Create a List that holds data.
        try {
            BufferedReader in = new BufferedReader(new FileReader(file));
            List<CompareUsers> UserLbTweets = new ArrayList<>();

            {
                String str = in.readLine();

                while (str != null) {
                    int polarity = 0;
                    BigInteger id = null;
                    String dateCoded = "";
                    String date = "";
                    String username = "";
                    String tweet = "";
                    String pol= "";

                    String[] str2Tokens;
                    str2Tokens = str.trim().split("\t");

                    pol = str2Tokens[0];
                    username = str2Tokens[4];
                    dateCoded = str2Tokens[2];
                    date = str2Tokens[3];
                    tweet = str2Tokens[5];

                    CompareUsers comp_users = new CompareUsers(username, dateCoded, date, tweet,pol);
                    UserLbTweets.add(comp_users);
                    str = in.readLine();
                }
            }
            return UserLbTweets;
        } catch (IOException ex) {
            Logger.getLogger(UserDates.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    
}
