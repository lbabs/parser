package parser;

import Constructors.CompareUsers;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Lefteris
 */
public class Parser {

    /**
     *
     * @param args Main
     * @throws ParseException
     * @throws FileNotFoundException
     * @throws FileNotFoundException
     * @throws MalformedURLException
     * @throws IOException
     * @throws org.apache.lucene.queryparser.classic.ParseException
     */
    public static void main(String[] args) throws ParseException, FileNotFoundException, FileNotFoundException, MalformedURLException, IOException, org.apache.lucene.queryparser.classic.ParseException {

        /**
         * Call UserDates to parse the "clean" data
         */
        UserDates userdates = new UserDates();
        List<CompareUsers> parseData = new ArrayList<>();
        float result = 0.0f;
        parseData = userdates.parseData("./data//data2.txt");

        /**
         * Call HelpClass to create a Map with users and its contents
         */
        HelpClass hc = new HelpClass();

        ValueDistance edit_distance = new ValueDistance();
        Map<String, List<CompareUsers>> mapUsers = hc.UserTweets(parseData);
        Set<String> keys = mapUsers.keySet();
        List<String> list = new ArrayList<>(keys);
        java.util.Collections.sort(list);

        /**
         * Start creating the file that would be the input of Weka for
         * clustering
         */
        String content = "@relation results" + "\n\n" + "@attribute user1 string" + "\n" + "@attribute user2 string\n" + "@attribute value1 numeric\n\n" + "@data\n\n";
        for (int i = 0; i < list.size(); i++) {
            for (int j = (i + 1); j < list.size(); j++) {
                //System.out.println("users:: " + list.get(i) + " -- " + list.get(j));
                result = edit_distance.compute(mapUsers.get(list.get(i)), mapUsers.get(list.get(j)));
                try {

                    /**
                     * Write the contents of the file
                     */
                    File file = new File("./results.arff");
                    content += list.get(i) + "," + list.get(j) + "," + result + "\n";
                    FileUtils.writeStringToFile(file, content);
                } catch (IOException e) {
                    System.err.println("Error occured " + e);
                }
            }
        }
    }
}